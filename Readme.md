# Performance Analisis

- A Jekyll page on gitlab
		[GitLab Jekyll Page](https://mindat11.gitlab.io/)
- GitLab Repo
		[All documents include the PDF and R script](https://gitlab.com/mindat11/EntregaB2)
- PDF file
		[Version of this document on PDF](https://gitlab.com/mindat11/EntregaB2/blob/master/Readme.pdf)
- R script
		[Raw version of R script](https://gitlab.com/mindat11/EntregaB2/blob/master/EntregaB2.R)
----

# Administration technological resources.

----

## 1. Problem definition

Make diagnoses and projections for server farms, require data analysis and processing of large amount of data, these processes can be not very efficient and usually do not have any support.

![Rack empty ](https://gitlab.com/mindat11/EntregaB2/raw/master/images/rack_empty.jpg)

----

### Consequences:

![Slow](https://gitlab.com/mindat11/EntregaB2/raw/master/images/slow.png)

- Problems in the operation due to delays in the processes derived from the use of the technological resource.
- Over-costs by over estimating the requirements of the technological resource.

----

### Causes:

The causes of incorrectly using the technological resource may be the lack of effective techniques in the estimation of current consumption.

----

### Goals:

Estimate and optimize the use of technological resources:

- Analyze the relationship between the consumption of resources and the management of operating system threads.
- Develop analysis techniques to optimize the use of the technological resource.

----

## 2. Selection of data sources for analysis

In general we will be based on consumption hystory, based on CPU, network, memory and storage consumption on Unix systems.

_
====

## Entry methods

- nmon files with basic performance data.


_
====

## Data source view

```R
host_proc <- read.csv("https://gitlab.com/mindat11/EntregaB2/raw/master/datos/host_list_proc_time.csv",
                  header=FALSE)
```

_
====

## Data source view

```R
head(host_proc)
  Type      pid  time pcpu pusr psys threads   size restext resdata carhio   ram paging command          wlm ZZZZ time2 real_time   date_time  unix_time     proc_name
1  TOP   393228 T0002 1.00 1.00 0.00       9    192       0     192      0 0.003      0    vmmd Unclassified ZZZZ T0002  00:15:15 20-AUG-2013 1376975715   vmmd:393228
2  TOP  5636338 T0002 0.42 0.25 0.18     240 449820     340  449744    545 7.154      0    java Unclassified ZZZZ T0002  00:15:15 20-AUG-2013 1376975715  java:5636338
3  TOP 14483668 T0002 0.39 0.29 0.10     156 359852     356  359776   6429 5.724      0    java Unclassified ZZZZ T0002  00:15:15 20-AUG-2013 1376975715 java:14483668
4  TOP 19660868 T0002 0.30 0.17 0.13     138 206648     340  206572    530 3.289      0    java Unclassified ZZZZ T0002  00:15:15 20-AUG-2013 1376975715 java:19660868
5  TOP 22085634 T0002 0.28 0.17 0.12     135 188060     340  187984    556 2.993      0    java Unclassified ZZZZ T0002  00:15:15 20-AUG-2013 1376975715 java:22085634
6  TOP  8061050 T0002 0.18 0.09 0.09     115 250236     340  250160   1637 3.982      0    java Unclassified ZZZZ T0002  00:15:15 20-AUG-2013 1376975715  java:8061050
```

_
====

## Data source view

```bash
 ls -1 | awk '{
   while ( "cat "$0" | grep -E \"^ZZZZ\" " | getline var) print var,$0;
 }'    | awk '{
   split($0,time_z,",");
   while ( "cat "$2" | grep "time_z[2]" " | getline var) print var","$1;
 }' | grep -E "^TOP" > ../host_list_proc_time.csv
```

----

## 3. Identification of analysis variables

In particular, we will look for the relationship that the consumption of resources has with the server processes, seeking to infer user response times, number of users and long-term tasks on the servers.

_
====

## Data source view

```R
# TOP,+PID,Time,%CPU,%Usr,%Sys,Threads,Size,ResText,ResData,CharIO,%RAM,Paging,Command,WLMclass
colnames(host_proc)<-c("Type",     # TOP        # 1
                       "pid",      # +PID       # 2
                       "time",     # Time       # 3
                       "pcpu",     # %CPU       # 4
                       "pusr",     # %Usr       # 5
                       "psys",     # %Sys       # 6
                       "threads",  # Threads    # 7
                       "size",     # Size       # 8
                       "restext",  # ResText    # 9
                       "resdata",  # ResData    # 10
                       "carhio",   # CharIO     # 11
                       "ram",      # %RAM       # 12
                       "paging",   # Paging     # 13
                       "command",  # Command    # 14
                       "wlm",      # WLMclass   # 15
                       "ZZZZ",     # No real data only I want to sleep a little   # 16
                       "time2",    # Temporal only for check, who cares!          # 17
                       "real_time", # Nice clock time                             # 18
                       "date_time") # Ugly date format                            # 19
```

_
====

See how different configurations respond to changes, seeking to understand server farm behavior in terms of a number of factors, including:

![dance](https://gitlab.com/mindat11/EntregaB2/raw/master/images/cpu1.jpeg)

- How many simultaneous processes use the system.
- What types of process threads are made.
- What kind of processes are executed on the server.

_
====

## Explore data

 Only 4 processes are permanent in the system during the time of the sample, this is important for research because although these processes are permanent, there are only 44 processes within the top of the system, which indicates that process "respawn" is not being presented.

![Frecuence](https://gitlab.com/mindat11/EntregaB2/raw/master/images/ProcessPer.png)

----

## 4. Determine the type of analysis and the possible data mining technique that you can apply

Mainly we will look for correlation analysis to determine how the resources are related to each other and then how they affect the processes and in general several views of the behavior of the same.

![Slow](https://gitlab.com/mindat11/EntregaB2/raw/master/images/correlations.jpeg)

_
====

## Correlation Analysis


When the correlation analysis is performed, it is identified that there is a direct relationship between the number of threads and the consumed memory, however the use of the CPU does not seem to be related to the number of threads.

![Slow](https://gitlab.com/mindat11/EntregaB2/raw/master/images/Correlation.png)

_
====

## Basic Regression Analysis

![Slow](https://gitlab.com/mindat11/EntregaB2/raw/master/images/predicted_basic.png)

_
====

## Regression Analysis

![Slow](https://gitlab.com/mindat11/EntregaB2/raw/master/images/kernel_regre.png)

_
====

## Neural Network Prediction

![Slow](https://gitlab.com/mindat11/EntregaB2/raw/master/images/NeuralN.png)

_
====

## Complete R Script.

```R
# Selection data from nmon source. Added unix_time column.
# ```bash
# ls -1 | awk '{
#   while ( "cat "$0" | grep -E \"^ZZZZ\" " | getline var) print var,$0;
# }'    | awk '{
#   split($0,time_z,",");
#   while ( "cat "$2" | grep "time_z[2]" " | getline var) print var","$1;
# }' | grep -E "^TOP" > ../host_list_proc_time.csv
# ```

# nmon data
rm(host_proc)
host_proc <- read.csv("https://gitlab.com/mindat11/EntregaB2/raw/master/datos/host_list_proc_time.csv",
                      header=FALSE)

# TOP,+PID,Time,%CPU,%Usr,%Sys,Threads,Size,ResText,ResData,CharIO,%RAM,Paging,Command,WLMclass
colnames(host_proc)<-c("Type",     # TOP        # 1
                       "pid",      # +PID       # 2
                       "time",     # Time       # 3
                       "pcpu",     # %CPU       # 4
                       "pusr",     # %Usr       # 5
                       "psys",     # %Sys       # 6
                       "threads",  # Threads    # 7
                       "size",     # Size       # 8
                       "restext",  # ResText    # 9
                       "resdata",  # ResData    # 10
                       "carhio",   # CharIO     # 11
                       "ram",      # %RAM       # 12
                       "paging",   # Paging     # 13
                       "command",  # Command    # 14
                       "wlm",      # WLMclass   # 15
                       "ZZZZ",     # No real data only I want to sleep a little   # 16
                       "time2",    # Temporal only for check, who cares!          # 17
                       "real_time", # Nice clock time                             # 18
                       "date_time") # Ugly date format                            # 19

head(host_proc)
tail(host_proc)
summary(host_proc)

# Calculate unix time
as.numeric(as.POSIXct("1969-12-31 19:00:00"))
as.numeric(as.POSIXct("2013-08-20 00:00:01"))
# [1] 1376974800
as.numeric(as.POSIXct("2013-08-20 00:15:15"))
# [1] 1376975715

# Año
substr( host_proc$date_time, 8, 11)
# Dia
substr( host_proc$date_time, 0, 2)

# Dificult time manage on R.
host_proc$unix_time<-as.numeric(as.POSIXct( strptime( paste( substr( host_proc$date_time, 8, 11), "-08-", substr( host_proc$date_time, 0, 2), host_proc$real_time ), "%Y -%m- %d%H:%M:%S") ))
host_proc$proc_name<-paste(host_proc$command,host_proc$pid,sep=":")

# Cheking data, tittle, sumary etc.
head(host_proc)
tail(host_proc)
str(host_proc)
summary(host_proc)
which(is.na(host_proc))

# Research and test of data.

library(ggplot2)

df <- data.frame(Doubles=double(),
                 Ints=integer(),
                 Factors=factor(),
                 Logicals=logical(),
                 Characters=character(),
                 stringsAsFactors=FALSE)

# Make a top of "prints"
rm(top_proc)
top_proc <- data.frame(pid=integer(),
                       frecuence=integer(),
                       stringsAsFactors=FALSE)

for ( my_pid in unique(  host_proc[['pid']] ) ){
  #  print( paste( "My pids:", my_pid, ":", length( which( host_proc[['pid']]==my_pid ) ) ) )
  proc_new<-c( my_pid , length( which( host_proc[['pid']]==my_pid ) ) )
  top_proc = rbind(top_proc ,proc_new)
}

colnames(top_proc)<-c( "pid",        # PID
                       "frecuence")  # frecuencia

rm(top_proc_10)
top_proc_10 <- head(top_proc, 20)

install.packages('ggplot2')
library(ggplot2)

ggplot( top_proc_10 ,aes(x=reorder(pid,frecuence),y=frecuence))+
  geom_bar(stat="identity",color="blue")+
  coord_flip()+
  geom_text(aes(label=frecuence),size=5)+
  ylab("Number of snapshot or \"prints\" ")+
  xlab("Pid Number")+
  ggtitle("Process Persistence on Time")

# The low number of processes 44 demuestra que no hay respawn en el sistema.

### Correlation Analysis

install.packages('PerformanceAnalytics')
library(PerformanceAnalytics)

corr<-host_proc[,7:12]
chart.Correlation(corr,histogram = TRUE,pch=19)

# The threads depends 1:1 of ram

summary(host_proc)
corr<-host_proc[,4:13]
chart.Correlation(corr,histogram = TRUE,pch=19)

## Basic Regression Analysis

#Create Model
model_threads<-lm(threads~pcpu+pusr+psys+size+restext+resdata+carhio+ram,data = host_proc)

#Summary of the model
summary(model_threads)

#Make Predictions
host_proc$pred_threads_lm<-predict(model_threads,newdata = host_proc)

#Root Mean Squared Error
rmse <- function(y, f) { sqrt(mean( (y-f)^2 )) }
rmse(host_proc$threads,host_proc$pred_threads_lm)

#Plot the prediction

ggplot(host_proc,aes(x=pred_threads_lm,y=threads))+
  geom_point(alpha=0.5,color="blue",aes(x=pred_threads_lm))+
  geom_smooth(aes(x=pred_threads_lm,y=threads),color="red")+
  geom_line(aes(x=threads,y=threads),color="black",linetype=2)+
  xlab("Predicted Thread")+ylab("Followed Thread")+
  ggtitle("Predicted Thread Behaviour")

# How many threads is better for my jobs.


## Support Vector Machine

library(kernlab)

#Create Model
kernl_model_trhead<-ksvm(threads~pcpu+pusr+psys+size+restext+resdata+carhio+ram,data = host_proc)

#Summary of the model
summary(kernl_model_trhead)

#Make Predictions
host_proc$pred_thread_svm<-predict(kernl_model_trhead,newdata = host_proc)

#Root Mean Squared Error
rmse(host_proc$threads,host_proc$pred_thread_svm)

#Plot the prediction
ggplot(host_proc,aes(x=pred_thread_svm,y=threads))+
  geom_point(alpha=0.5,color="blue")+
  geom_smooth(aes(x=pred_thread_svm,y=threads),color="red")+
  geom_line(aes(x=threads,y=threads),color="black",linetype=2)+
  xlab("Predicted Thread")+ylab("Followed Thread")+
  ggtitle("Predicted Thread Behaviour")


## Neural Network

library(nnet)

#Create Model
model_nnet<-nnet(threads~pcpu+pusr+psys+size+restext+resdata,data = host_proc,size=12,maxit=500,linout = T,decay = 0.01)

#Summary of the model
summary(model_nnet)

#Make Predictions
x<-host_proc[,3:10]
y<-host_proc[,7]

host_proc$pred_prp_nnet<-predict(model_nnet,x,type="raw")

#Root Mean Squared Error
rmse(host_proc$threads,host_proc$pred_prp_nnet)

#Plot the prediction
ggplot(host_proc,aes(x=pred_prp_nnet,y=threads))+
  geom_point(alpha=0.5,color="black")+
  geom_smooth(aes(x=pred_prp_nnet,y=threads),color="brown")+
  geom_line(aes(x=threads,y=threads),color="blue",linetype=2)+
  xlab("Predicted Performance")+ylab("Published performance")+
  ggtitle("Neural Network regression Analysis of Computer Performance")
```

----

### Reference links

- [Setting up Automated PHP Testing on GitLab CI](https://w3guy.com/php-testing-gitlab-ci/)
- [How to Setup PHPunit Code Coverage in GitLab](https://w3guy.com/phpunit-code-coverage-gitlab/)
- [Testing PHP projects](https://docs.gitlab.com/ce/ci/examples/php.html)
- [KPI Analysis](https://www.anodot.com/blog/kpi-monitoring-with-ai/)
-
- [Monitor your Infrastructure with Elastic Beats](https://www.youtube.com/watch?v=5Z028ubqpt0)
- [Machine Learning Elastic](https://www.elastic.co/products/x-pack/machine-learning)
- [Anomaly Detection using Elasticsearch](https://www.youtube.com/watch?v=bmdBh8Xq8yM)

_
====

- [Diseño e Implementación de una solución de gestion centralizada de logs de aplicaciones](http://openaccess.uoc.edu/webapps/o2/bitstream/10609/42250/3/bycaTFM0615memoria.pdf)
- [Ubuntu: Enabling syslog on Ubuntu and custom templates](https://fabianlee.org/2017/05/24/ubuntu-enabling-syslog-on-ubuntu-and-custom-templates/)
- [Using rsyslog to send pre-formatted JSON to logstash](https://untergeek.com/2012/10/11/using-rsyslog-to-send-pre-formatted-json-to-logstash/)
- [Docker Logging with the ELK Stack](https://logz.io/blog/docker-logging/)
- [Evolutionary artificial neural network](https://is.muni.cz/th/389924/fi_m/eann.pdf)
- [Analisis datascience](http://www.datascience-zing.com/blog/implementation-of-19-regression-algorithms-in-r-using-cpu-performance-data)
- [docker-elk](https://github.com/deviantony/docker-elk)
- [how-to-centralize-logs-with-rsyslog-logstash-and-elasticsearch](https://www.elastic.co/blog/how-to-centralize-logs-with-rsyslog-logstash-and-elasticsearch-on-ubuntu-14-04)

_
====

- [Discovering Your Data](https://www.elastic.co/guide/en/kibana/current/tutorial-discovering.html)
- [How to Configure the syslogd Logstash Input](http://blog.eagerelk.com/how-to-configure-the-syslogd-logstash-input/)

_
====

- [metricbeat](https://www.elastic.co/downloads/beats/metricbeat)
- [Metric Analytics with Metricbeat](https://www.packtpub.com/books/content/metric-analytics-metricbeat)
- [Public Repo data](https://archive.ics.uci.edu/ml/datasets/Computer+Hardware)
- [Old dog, New tricks](http://www.idata.co.il/2017/01/monitoring-systems-with-elasticsearch-and-metricbeat/)

_
====

- [ropensci](https://github.com/ropensci/elastic)
